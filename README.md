# Spring Security Sample

**환경구성**

- java 17
- gradle
- spring boot 2.7.11

**작업목표**

- deprecated된 class들을 대체하여 안정성이 높아진 Spring Security 설정 실습
- 주로 Maven을 사용했지만 Gradle을 사용해보고자 함.